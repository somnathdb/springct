const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const db = `mongodb+srv://rest-api:rest-api@cluster0.uouvd.mongodb.net/SpringCT?retryWrites=true&w=majority`;

mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log('Database sucessfully connected');
}).catch((err) => {
    console.log(error);
});
app.use(
    bodyparser.urlencoded({
        extended: false
    })
);
app.use(bodyparser.json());
const companyRoutes = require('./server/routes/companyRoutes');
const userRoutes = require('./server/routes/userRoute');
app.use('/company', companyRoutes);
app.use('/user', userRoutes);
// app.get('/test',(req,res,next)=>{
//     res.status(200).json({
//         message:'sucess'
//     })
// })

app.listen(8001, (req, res, next) => {
    console.log('Server Successfully Start');
})